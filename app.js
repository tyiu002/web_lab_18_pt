// Setup code
// --------------------------------------------------------------------------
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Specify that the app should use fs
var fs = require('fs');

// Specify that the app should use node-datetime
var dateTime = require('node-datetime');

// Specify that the app should use body parser (for reading submitted form data)
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true })); // Allows us to read forms submitted with POST requests

// Specify that the app should use cookie-parser to read / write cookies. Omnomnom.
var cookieParser = require('cookie-parser');
app.use(cookieParser());

// Specify that the app should use express-session to create in-memory sessions
var session = require('express-session');
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: "compsci719"
}));

// Load data from animals file - its content is unchanging so we can do this.
var animals = require("./animals.json");

// --------------------------------------------------------------------------


// Passport setup code
// --------------------------------------------------------------------------

// Specify that the app should use "passport" for authentication, and
// that the authentication type should be "local".
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

// Load user data from a file. We can import JSON files like this, as long as we never need to change their contents.
var users = require("./users.json");

// A function to get a user with the given username from the "users" data.
// Will return the required user, or undefined.
function findUser(username) {
    /*return users.find(function (user) {
        return user.username == username;
    });*/
    for (var i = 0; i < users.length; i++) {
        if (users[i].username == username) {
            return users[i];
        }
    }
    return undefined;
}

// Set up local authentication
var localStrategy = new LocalStrategy(
    function (username, password, done) {

        // Get the user from the "database"
        user = findUser(username);

        // If the user doesn't exist...
        if (!user) {
            return done(null, false, { message: 'Invalid user' });
        };

        // If the user's password doesn't match the typed password...
        if (user.password !== password) {
            return done(null, false, { message: 'Invalid password' });
        };

        // If we get here, everything's OK.
        done(null, user);
    }
);

// This method will be called when we need to save the currently
// authenticated user's username to the session.
passport.serializeUser(function (user, done) {
    done(null, user.username);
});

// This method will be called when we need to get all the data relating
// to the user with the given username.
passport.deserializeUser(function (username, done) {
    user = findUser(username);
    done(null, user);
});

// Set up Passport to use the given local authentication strategy
// we've defined above.
passport.use('local', localStrategy);

// Start up Passport, and tell it to use sessions to store necessary data.
app.use(passport.initialize());
app.use(passport.session());

// A helper function, which will check if there's a user that's logged in.
// If there is an authenticated user, then "next()" will be called,
// which will forward the request onto the actual page to be displayed.
// If there is no authenticated user, then they'll be redirected to
// the homepage.
// See below for an example of how to use this function.
function isLoggedIn(req, res, next) {
    // if user is authenticated, carry on 
    if (req.isAuthenticated()) {
        return next();
    }

    // redirect them to the home page
    res.redirect("/login");
}
// --------------------------------------------------------------------------


// Route handlers
// --------------------------------------------------------------------------

// If the user navigates to "/" or "/animals", display the animals page.
app.get(["/", "/animals"], function (req, res) {

    var username = null;
    if (req.isAuthenticated()) {
        username = req.user.username;
    }

    var data = {
        thisPage: "/",
        animalsPage: true,
        animals: animals,
        username: username,
        loggedOut: req.query.loggedOut,
        cartMessage: req.query.cartMessage,
        cart: loadCart(req, res)
    }

    res.render("animals", data);
});

// If the user navigates to "/login", if the user is already authenticated,
// redirect them to the /orderHistory page. Otherwise, render the login page.
app.get("/login", function (req, res) {
    if (req.isAuthenticated()) {
        res.redirect("/orderHistory");
    }
    else {
        // This data will be used to display helpful error / info panels on the homepage.
        var data = {
            layout: "no-navbar", // The login page doesn't use the navbar / shopping cart.
            loginFail: req.query.loginFail // Will be set when the user is redirected here upon failing to login.
        }
        res.render("login", data);
    }
});

// If the user POSTs to "/login", process their authentication attempt. If it succeeds,
// redirect them to "/orderHistory". If it fails, redirect them to the login page.
app.post('/login', passport.authenticate('local',
    {
        successRedirect: '/orderHistory',
        failureRedirect: '/login?loginFail=true'
    }
));

// If the user navigates to "/logout", log them out and then redirect them
// to the animals page.
app.get("/logout", function (req, res) {
    req.logout();
    res.redirect("/animals?loggedOut=true");
});

// If the user POSTs to /updateCart, update the user's cart according to the form data, then
// redirect to whatever page the user just came from.
app.post("/updateCart", function (req, res) {

    // Get the user's cart
    var cart = loadCart(req, res);

    // Update it according to the submitted form data
    for (var i = 0; i < cart.length; i++) {
        var inputName = "count-" + cart[i].id;
        var newCount = parseInt(req.body[inputName]);
        cart[i].count = newCount;
    }

    // Delete items in cart which now have a count of 0 or less.
    cart = cart.filter(function (item) {
        return item.count > 0;
    });

    // Save the user's cart
    saveCart(cart, req, res);

    // Redirect to wherever the user just came from
    res.redirect(req.body.thisPage + "?cartMessage=You have successfully updated your cart.");

});

// If the user POSTs to "/addToCart", add the specified animals to the cart, and then redirect to
// the main page.
app.post("/addToCart", function (req, res) {

    // Get submitted form data
    var num = parseInt(req.body.amount);
    var animalId = parseInt(req.body.animalId);
    /*var animal = animals.find(function (item) {
        return item.id == animalId;
    })*/
    var animal;
    for (var i = 0; i < animals.length; i++) {
        if (animalId == animals[i].id) {
            animal = animals[i];
            break;
        }
    }
    var animalName = animal.name;

    // Get the user's cart and update it
    var cart = loadCart(req, res);
    /*var cartItem = cart.find(function (item) {
        return item.id == animalId;
    });*/
    var cartItem;
    for (var i = 0; i < cart.length; i++) {
        if (animalId == cart[i].id) {
            cartItem = cart[i];
            break;
        }
    }

    // If this is the first time we've added this animal..
    if (cartItem == undefined) {
        cartItem = { id: animalId, name: animalName, count: 0 };
        cart.push(cartItem);
    }

    // Update the item
    cartItem.count += num;

    // Save the user's cart
    saveCart(cart, req, res);

    // Redirect to wherever the user just came from
    res.redirect("/?cartMessage=You added " + num + " " + animalName + " to your cart.");
});

// If the user POSTs to "/clearCart", delete the user's cart then redirect..
app.post("/clearCart", function (req, res) {

    // Delete the "cart" cookie.
    deleteCart(req, res);

    // Redirect to wherever the user just came from
    res.redirect(req.body.thisPage + "?cartMessage=You successfully cleared your cart.");
});

// If the user navigates to "/orderHistory", if the user is not authenticated,
// redirect them to the login page (handled by isLoggedIn). Otherwise,
// render the welcome page, which displays the user's name.
app.get("/orderHistory", isLoggedIn, function (req, res) {

    var data = {
        thisPage: "/orderHistory",
        orderHistoryPage: true,
        orderSuccess: req.query.orderSuccess,
        cart: loadCart(req, res),
        username: req.user.username,
        orders: loadOrderHistoryFor(req.user.username)
    };

    res.render("orderHistory", data);
});

// If the user navigates to "/checkout", if the user is not authenticated, redirect them to the
// login page (handled by isLoggedIn). Otherwise, render the checkout page.
app.get("/checkout", isLoggedIn, function (req, res) {

    var data = {
        thisPage: "/checkout",
        checkoutPage: true,
        cart: loadCart(req, res),
        username: req.user.username,
        order: createOrder(req, res)
    };

    res.render("checkout", data);

});

// If the user POSTs to "/checkout", if the user is not authenticated, redirect them to the
// login page (handled by isLoggedIn). Otherwise, complete the order.
app.post("/checkout", isLoggedIn, function (req, res) {

    // Get the order to "buy"
    var order = createOrder(req, res);

    // Add it to the currently authenticated user's orders
    addOrderFor(req.user.username, order);

    // Clear the shopping cart
    deleteCart(req, res);

    // Redirect to order history page.
    res.redirect("/orderHistory?orderSuccess=true");

});

// Make "/public" accessible
app.use(express.static(__dirname + "/public"));

// If the user browses anywhere else, give them a 404.
app.use(function (req, res) {
    res.status(404);
    var data = {
        layout: "no-navbar",
        errorMessage: "404 - Page not Found"
    }
    res.render("error", data);
});

// --------------------------------------------------------------------------


// Load / Save Shopping Cart
// --------------------------------------------------------------------------

// Loads the shopping cart from cookies / sessions
function loadCart(req, res) {

    var cart = [];

    if (req.cookies && req.cookies.cart) {
        cart = req.cookies.cart;
    }

    return cart;
}

// Saves the shopping cart to cookies / sessions
function saveCart(cart, req, res) {
    res.cookie("cart", cart);
}

// Deletes the shopping cart from cookies / sessions
function deleteCart(req, res) {
    res.clearCookie("cart");
}
// --------------------------------------------------------------------------


// Order processing
// --------------------------------------------------------------------------

// Creates an order from the user's current shopping cart.
function createOrder(req, res) {
    // Get shopping cart
    var cart = loadCart(req, res);

    // Calculate total price of items in cart
    var totalCost = 0;
    for (var i = 0; i < cart.length; i++) {
        var cartItem = cart[i];
        /*var animal = animals.find(function (a) {
            return a.id == cartItem.id;
        });*/
        var animal;
        for (var i = 0; i < animals.length; i++) {
            if (cartItem.id == animals[i].id) {
                animal = animals[i];
                break;
            }
        }
        totalCost += (animal.price * cartItem.count);
    }

    // Create a nice-looking date
    var dt = dateTime.create();

    var order = {
        items: cart,
        totalCost: totalCost,
        timestamp: dt.format("Y-m-d H:M")
    };

    return order;
}

// Loads an array of all orders for the given user.
function loadOrderHistoryFor(username) {
    var allOrders = JSON.parse(fs.readFileSync(__dirname + "/order-history.json"));
    var ordersForUser = allOrders[username];
    if (ordersForUser) {
        return ordersForUser;
    }
    else {
        return []; // If there's no order history for a given user, return the empty array.
    }
}

// Saves an array of all orders for the given user.
function saveOrderHistoryFor(username, orderHistory) {
    var allOrders = JSON.parse(fs.readFileSync(__dirname + "/order-history.json"));
    allOrders[username] = orderHistory;
    fs.writeFileSync(__dirname + "/order-history.json", JSON.stringify(allOrders));
}

// Adds a single order for the given user.
function addOrderFor(username, order) {
    var userOrders = loadOrderHistoryFor(username);
    // "unshift" method adds items to the beginning of an array: https://www.w3schools.com/jsref/jsref_unshift.asp
    userOrders.unshift(order);
    saveOrderHistoryFor(username, userOrders);
}
// --------------------------------------------------------------------------


// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});